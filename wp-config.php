<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'apiwp' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sgzJ2g]_@wJV R$a^0]]}o87[VX`en%&9(~t^)A8f%i4AENCy*J =sG6HS!.;sZ#' );
define( 'SECURE_AUTH_KEY',  '<OgZ[h[.Jy&<opCl]JYO1_dCjvG`E?M;tERrYQJS..NM1&+Dy5{R[x6j4dkYbyPF' );
define( 'LOGGED_IN_KEY',    'lI@Z;8^S|g/5N9H@^GOOBw$7u&[<v:NPV^~IU#e0v/9l4j3|]N0&e!9bi/v[wa!E' );
define( 'NONCE_KEY',        '@KxdTBj??!~||672<Z2kAQA][eEEH+6)2l%x;2)Hrl1%sp,fnA8!2l_BvwK=#kw9' );
define( 'AUTH_SALT',        'I3`p5Mv:Aq=8ED)5v<!16T8&X8wD{WrFH[C2gT)+nhVi-uTo{FHThhwsEIo(/H%t' );
define( 'SECURE_AUTH_SALT', '@MCalNJEThym3K~_Fy=Zr!k$9^AFkx_*{[c hhltqpe-G%oU*,EiedoQ>9lDd.Qj' );
define( 'LOGGED_IN_SALT',   'X}IbdKIty+5SqFF?P886?ZrV;0,v-8RL00z{v{~.)az}.%| Q$x!LVsQm|066t25' );
define( 'NONCE_SALT',       'Ryg6a&qPS^d7$h`/lJG4sg1sgaKl_]McuM2Yw*F#eBlyvR#>3eqc}(XvU H486nO' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
